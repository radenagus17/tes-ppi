// event pada saat link di klik
$('.page-scroll').click(function(e){
    
    // ambil isi href
    var tujuan = $(this).attr('href');
    var elementTujuan = $(tujuan);

    // pindahkan scroll
    $('html, body').animate({
        scrollTop: elementTujuan.offset().top - 50
    },1250, 'easeInOutBack');

});

// $('.page-scroll').click(function(e){
//     $('html, body').animate({
//         scrollTop: $($(this).attr('href')).offset().top - 50
//     }, 1000);
// });


// paralax
// about
$(window).on('load', function(){
    $('.pKiri').addClass('pMuncul');
    $('.pKanan').addClass('pMuncul');
});     

$(window).scroll(function(){
    var wScroll = $(this).scrollTop();
    
    // jumbotron
    $('.jumbotron img').css({
        'transform': 'translate(0px, '+ wScroll/4 +'%)'
    })

    $('.jumbotron h1').css({
        'transform': 'translate(0px, '+ wScroll/2 +'%)'
    })

    $('.jumbotron p').css({
        'transform': 'translate(0px, '+ wScroll/1.2 +'%)'
    })

    // portofolio
    if(wScroll > $('.portofolio').offset().top - 200){
        $('.portofolio .thumbnail').each(function(i){
            setTimeout(function(){
                $('.portofolio .thumbnail').eq(i).addClass('muncul');
            }, 500 * (i+1))
        })
        
    }
});